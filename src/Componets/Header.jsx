import { Flex, Spacer,InputGroup,InputLeftElement,Input,Image} from '@chakra-ui/react'
import { SearchIcon } from '@chakra-ui/icons'
import HeaderMenu from '../Componets/HeaderMenu'
import AuthenticationSection from './AuthenticationSection'
import {Link} from "react-router-dom"
const Header=()=>{
    return(
        <Flex justifyContent={"space-between"} alignContent={"center"} textAlign={"center"} maxW={"95%"} mx="auto" my="20px">
            <Link to="/"><Image src={"https://logowik.com/content/uploads/images/570_reddit.jpg"} alt="header-logo" h="50px" p="0px"/></Link>
            <HeaderMenu w="100px"/>
            <InputGroup w="70%">
                <InputLeftElement  pointerEvents="none" color="gray.300" fontSize="1.2em" children={<SearchIcon ml="4px"/>}/>
                <Input placeholder="search .." />
            </InputGroup>
            <AuthenticationSection />
        </Flex>
    )
}

export default Header;