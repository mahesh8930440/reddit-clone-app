import React, { useState } from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
  Heading,
  Text,
  Input,
  StepStatus,
  Box,
  Textarea
} from '@chakra-ui/react';
import { useSelector, useDispatch } from 'react-redux'
import { useUpdateCommentMutation } from '../Redux/CreateCommentApi';
import { useNavigate } from 'react-router-dom';
import {  handleClosingCommentModal} from "../Redux/CommentModalSlice"
import { auth } from '../firebaseAuthentication';
const CommentModal=({comment})=>{
    // console.log(id)
    const user=auth.currentUser;
    
    const [inputValue,setInputValue]=useState(comment.description);
    const modalStatus = useSelector((state) => state.commentModal.commentModalStatus)
    const [isLoading,setIsLoading]=useState(false);
    const dispatch = useDispatch()
    const [updateComment]=useUpdateCommentMutation();
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [errorMessage,setErrorMessage]=useState("");
    const handleUpdateComment=async (id)=>{
        try{
            if (!navigator.onLine) {
              setErrorMessage(
                "You are currently offline. Please check your network connection."
              );
              return;
            }
            if (!inputValue) {
              setErrorMessage("comment is required");
              return;
            } 
            setErrorMessage("")
            setIsLoading(true);
            await updateComment({id,inputValue});
            dispatch(handleClosingCommentModal());
            setInputValue("");
        }
        catch(err){
            console.log("An error occurred",err.message)
            setErrorMessage(err.message)
          }  
    }
    return (
        <Modal isOpen={modalStatus} onClose={() => dispatch(handleClosingCommentModal())} size="lg">
          <ModalOverlay />
          <ModalContent>
            <ModalHeader >Edit Comment</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              {user && <Text mb='8px'>comment as    <Box as="span" color="pink">{user.displayName}</Box></Text>}  
              <Textarea
                        value={inputValue}
                        onChange={(e)=>  {setInputValue(e.target.value),setErrorMessage("")}}
                        placeholder='what are you thoughts ?'
                        size="md"
                        mb="40px"
                        variant='filled'
                    />
            </ModalBody>
            <ModalBody>
                {errorMessage && <Text color={"red"} ml="1%">{errorMessage}</Text>}
            </ModalBody>
            <ModalFooter justifyContent={"space-between"}>
              
              <Button variant="ghost" mr={"10px"} ml="0px" 
                onClick={() => dispatch(handleClosingCommentModal())}>
                Close
              </Button>
              <Button  colorScheme="blue"  onClick={()=>handleUpdateComment(comment.commentId)} loadingText='Submitting' isLoading={isLoading}>update</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      );
}
export default CommentModal;