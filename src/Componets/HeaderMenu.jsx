import {
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    Button,
    Icon,
    Flex,
    Heading,
    Text
} from '@chakra-ui/react'
import { FaHome } from "react-icons/fa";
import { ChevronDownIcon } from '@chakra-ui/icons'
import {Link} from "react-router-dom"
import { IoAdd } from "react-icons/io5"
import { handleOpeningModal } from '../Redux/ModalSlice';
import { useSelector, useDispatch } from 'react-redux'
import CommunityModal from './CommunityModal';
import { useGetCommunityQuery } from '../Redux/subRedditApi';
const HeaderMenu =()=>{
    const modalStatus = useSelector((state) => state.modal.modalStatus)
    const dispatch = useDispatch()
    const {data,isError, error} =useGetCommunityQuery();
    
    return(
        <Menu>
        <MenuButton as={Button} rightIcon={<ChevronDownIcon ml="40px"/>}   >
            <Flex textAlign={"center"} alignContent={"center"}>
                <Icon as={FaHome} boxSize={6} color="black" mr="10%"/>
                <span  >Home</span>
            </Flex>
        </MenuButton>
        <MenuList>
        <MenuItem minH='48px'>
            <IoAdd />
            <span ml="30px" onClick={()=>dispatch(handleOpeningModal())}>Create Community</span>
            {modalStatus && (<CommunityModal />)}
        </MenuItem>
        <MenuList minH='40px'>
                <Flex display="column">
                    <Heading mb="10px" fontSize={"1rem"}>YOUR COMMUNITIES</Heading>
                    {data && (
                       <Flex direction="column">
                       {data ?.map((community) => (
                            <Link to={`/r/${community.name}`} key={community.name}>
                                <MenuItem>
                                    <Text width="100%" display="block" p="8px" >
                                        {community.name}
                                    </Text>
                                </MenuItem>
                            
                            </Link>
                        ))}
                     </Flex>
                     
                    )}
                </Flex>
                

            </MenuList>
        <MenuItem minH='40px'>
            <IoAdd />
            <span ml="30px">Create Post</span>
        </MenuItem>
        </MenuList>
    </Menu>)
}


export default HeaderMenu;