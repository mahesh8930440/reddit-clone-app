import { FirebaseContext } from '../firebaseAuthentication';
import {useContext} from "react";

import {Menu,MenuButton,MenuItem,Button,MenuList,Text, Avatar,Flex,useToast} from '@chakra-ui/react'
import { GoSignOut } from "react-icons/go";
const AuthenticationSection= ()=>{
    const authData =useContext(FirebaseContext);
    const toast=useToast();
    const user=authData.user;
    const outline= {
          color:"gray",
          border: "1px solid",
          borderColor: "blue.500",
          borderRadius:"10px",
          padding:"4px",
          margin :"4px"
    }
    return (
        <>
            {user ? (
                <Menu width="80px">
                    <MenuButton as={Button}  pr="15px" pl="0px" flexWrap={"wrap"}>
                        <MenuItem><Avatar src={"https://images.pexels.com/photos/1704488/pexels-photo-1704488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"} height="20px" width="20px" mr="5px">
                            </Avatar>
                            <Text>{user.displayName}</Text>
                            </MenuItem>
                    </MenuButton>
                    <MenuList>
                        <MenuItem minH='48px'>
                            <GoSignOut mr="12px" size="30px"/>
                            <Button onClick={authData.handleSignOut}>Sign Out</Button>
                        </MenuItem>
                    </MenuList>
                </Menu>
            ) : (
                <button onClick={authData.signInWithGoogle} style={outline}>Sign In</button>
            )}
        </>
    );
    
}
export default AuthenticationSection;