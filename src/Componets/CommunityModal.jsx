import React, { useState } from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Button,
  Heading,
  Text,
  Input,
  StepStatus,
  
} from '@chakra-ui/react';
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom';
import {  handleClosingModal} from '../Redux/ModalSlice'
import { useAddCommunityMutation } from '../Redux/subRedditApi';
const CommunityModal = () => {
  const navigate = useNavigate();
  const [addCommunity] =useAddCommunityMutation();
  const modalStatus = useSelector((state) => state.modal.modalStatus)
  const [inputValue,setInputValue]=useState("");
  
  const [isLoading,setIsLoading]=useState(false);
  const dispatch = useDispatch()
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [errorMessage,setErrorMessage]=useState("");
  const newSubReddit=async()=>{
    try{
      if (!navigator.onLine) {
        setErrorMessage(
          "You are currently offline. Please check your network connection."
        );
        return;
      }
      if (!inputValue) {
        setErrorMessage("Community name is required");
        return;
      } 
      setErrorMessage("")
      setIsLoading(true);
      try {
        let result = await addCommunity(inputValue);
        console.log(result)
        if (result.data) {
          dispatch(handleClosingModal());
          navigate(`/r/${inputValue}`, { state: inputValue });
          setInputValue("");
        } else {
          throw new Error(result.error.message);
        }
      } catch (error) {
        console.error(error);
        throw new Error(error)
        
      } finally {
        setIsLoading(false);
      }
      
    }
    catch(err){
      console.log("An error occurred",err.message)
      setErrorMessage(err.message)
    }  
  }
  
  return (
    <Modal isOpen={modalStatus} onClose={() => dispatch(handleClosingModal())} size="lg">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader >Create Community</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
           <Heading fontSize={"2rem"} mb="10px">Name</Heading>
           <Text mb="30px">Community names including capitalization cannot be changed.</Text>
           <Input variant='filled' type="text" placeholder='Enter new SubReddit' value={inputValue} mb="20px" size='md' p="40px" height="20px" onChange={(e) => {setInputValue(e.target.value),setErrorMessage("")}}/>
           
        </ModalBody>
        <ModalBody>
            {errorMessage && <Text color={"red"} ml="1%">{errorMessage}</Text>}
        </ModalBody>
        <ModalFooter justifyContent={"space-between"}>
          
          <Button variant="ghost" mr={"10px"} ml="0px" 
            onClick={() => dispatch(handleClosingModal())}>
            Close
          </Button>
          <Button  colorScheme="blue"  onClick={()=>newSubReddit()} loadingText='Submitting' isLoading={isLoading}>Create</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default CommunityModal;