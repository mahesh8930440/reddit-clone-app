import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  modalStatus: false,
};

export const ModalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    handleOpeningModal: (state) => {
      state.modalStatus = true;
    },
    handleClosingModal: (state) => {
      state.modalStatus = false;
    },
  },
});

export const { handleOpeningModal, handleClosingModal } = ModalSlice.actions;

export default ModalSlice.reducer;
