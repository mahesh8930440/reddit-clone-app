import { createApi,fakeBaseQuery } from "@reduxjs/toolkit/query/react";
import { collection ,getDocs,doc,updateDoc,setDoc,query,where} from 'firebase/firestore';
import { auth } from '../firebaseAuthentication';
import {db} from "../firebaseAuthentication"
export const updateVoteApi=createApi({
    reducerPath:"updateVoteApi",
    baseQuery:fakeBaseQuery(),
    tagTypes:["updateVote"],
    endpoints:(builder)=>({
        updateVoteCount:builder.mutation({
            async queryFn({newVoteType, postId, voteCount}){
                try {
                    const user=auth.currentUser;
                    if (!user) {
                      console.log("User not logged in");
                      return;
                    }
              
                    const voteQuery = query(
                      collection(db, "votes"),
                      where("userId", "==", user.uid),
                      where("postId", "==", postId)
                    );
              
                    const voteSnapshot = await getDocs(voteQuery);
                    if (!voteSnapshot.empty) {
                      const voteData = voteSnapshot.docs[0].data();
              
                      const voteRef = doc(db, "votes", user.uid + postId);
                      
                      if (newVoteType === voteData.voteType) {
                        const newVoteCount = newVoteType === "upVote" ? -1 : 1;
              
                        const updatedVoteCount = voteCount + newVoteCount;
              
                        await updateDoc(voteRef, { voteType: null });
                        
                        const postRef = doc(db, "posts", postId);
              
                        await updateDoc(postRef, {
                          voteCount: updatedVoteCount,
                          voteType:null
                        });
                        
                      } else if (voteData.voteType === null) {
                        const newVoteCount = newVoteType === "upVote" ? 1 : -1;
                        const updatedVoteCount = voteCount + newVoteCount;
              
                        await updateDoc(voteRef, { voteType: newVoteType });
                        
                        const postRef = doc(db, "posts", postId);
              
                        await updateDoc(postRef, {
                          voteCount: updatedVoteCount,
                          voteType:  newVoteType
                        });
                       
                      } else {
                        const newVoteCount = newVoteType === "upVote" ? 2 : -2;
                        const updatedVoteCount = voteCount + newVoteCount;
              
                        await updateDoc(voteRef, { voteType: newVoteType });
                        
                        const postRef = doc(db, "posts", postId);
              
                        await updateDoc(postRef, {
                          voteCount: updatedVoteCount,
                          voteType:  newVoteType
                        });
                        
                      }
                    } else {
                      const newVoteCount = newVoteType === "upVote" ? 1 : -1;
                      const updatedVoteCount = voteCount + newVoteCount;
                     
                      const voteId = user.uid + postId;
                      const ref = doc(db, "votes", voteId);
                      await setDoc(ref, {
                        userId: user.uid,
                        postId: postId,
                        voteType: newVoteType,
                        voteId,
                      });
                      const postRef = doc(db, "posts", postId);
                      await updateDoc(postRef, {
                        voteCount: updatedVoteCount,
                        voteType:  newVoteType
                      });
                    }
                    
                   return {data:"ok"}
                  } catch (err) {
                    console.error("Error handling vote:", err);

                    return {error:err}
                  }
            
              
            },
            invalidatesTags:["updateVote"]
        })
    })
})
export const {useUpdateVoteCountMutation}=updateVoteApi;