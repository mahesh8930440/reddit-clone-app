import { fakeBaseQuery,createApi } from "@reduxjs/toolkit/query/react";
import { collection ,getDocs,doc,updateDoc, serverTimestamp,setDoc,query,getDoc,where} from 'firebase/firestore';
import {db} from "../firebaseAuthentication"
import { auth } from '../firebaseAuthentication';
export const createCommentApi=createApi({
    reducerPath:"createCommentApi",
    baseQuery:fakeBaseQuery(),
    tagTypes:["createComment"],
    endpoints:(builder)=>({
        fetchComment:builder.query({
           async queryFn(postId){
                try{
                    const commentQuery = query(
                    collection(db, "comment"),
                    where("postId", "==", postId)
                    );
                
                    const commentSnapshot = await getDocs(commentQuery);
                    const commentsData=commentSnapshot.docs.map((comment)=>comment.data())
                    console.log(commentsData)
                    return {data:commentsData}
                }
                catch(err){
                    console.log(err)
                    return{error:err}
                }            
           },
           providesTags:["createComment"]
        }),
        fetchPost:builder.query({
            async queryFn(postId){
                try{
                   const docRef = doc(db, "posts", postId);
                    const postSnapShot= await getDoc(docRef);
                    if (postSnapShot.exists()) {
                        const postData = postSnapShot.data();
                        // console.log(postData);
                        return { data: postData };
                      } else {
                        console.log("Post not found");
                        return { error: "Post not found" };
                      }
                }
                catch(err){
                    console.log("Error occurred :",err)
                    return {error:err}
                }
            },
            providesTags:["createComment"]
        }),
        addComments:builder.mutation({
            async queryFn({post,inputValue}){
                try{
                    const user=auth.currentUser;
                    const {userId,postId,subredditName}=post;
                    const commentId=new Date().getTime().toString();
                    console.log(commentId)
                    await setDoc(doc(db,"comment",commentId),{
                        userId,
                        postId,
                        user:user.displayName,
                        commentId,
                        description:inputValue,
                        subredditName,
                        createdAt:serverTimestamp(),
                        editedAt:serverTimestamp()
                    })
                    
                    const postRef = doc(db, "posts", postId);
                    let result=await updateDoc(postRef, {
                    commentCount :post.commentCount+1, 
                    });
                    console.log(result)
                    return {data:"successful"}
                }
                catch(err){
                    console.log("Error occurred:",err)
                    return {error:err}
                }
            },
            invalidatesTags:["createComment"]
        }),
        updateComment:builder.mutation({
            async queryFn({id,inputValue}){
                try{
                    const commentRef = doc(db, "comment", id);
                    await updateDoc(commentRef, {
                        description:inputValue
                    });
                    return {data:"ok"}
                }
                catch(err){
                   console.log(err)
                   return {error:err}
                }
                
            },
            invalidatesTags:["createComment"]
        })

    })
})
export const {useFetchCommentQuery,useFetchPostQuery,useAddCommentsMutation,useUpdateCommentMutation}=createCommentApi;