import { configureStore } from '@reduxjs/toolkit'
import  ModalSlice  from './ModalSlice';
import CommentModalSlice from './CommentModalSlice';
import { communityApi } from './subRedditApi';
import { setupListeners } from '@reduxjs/toolkit/query'
import { createPostApi } from './CreatePostApi';
import { createCommentApi } from './CreateCommentApi';
import { updateVoteApi } from './CreateVoteApi';
export const store = configureStore({
  reducer: {
    modal:ModalSlice,
    commentModal:CommentModalSlice,
    [communityApi.reducerPath]:communityApi.reducer,
    [createPostApi.reducerPath]:createPostApi.reducer,
    [createCommentApi.reducerPath]:createCommentApi.reducer,
    [updateVoteApi.reducerPath]:updateVoteApi.reducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(communityApi.middleware,createPostApi.middleware,createCommentApi.middleware,updateVoteApi.middleware),
});
setupListeners(store.dispatch)