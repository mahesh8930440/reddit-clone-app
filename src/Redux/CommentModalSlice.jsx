import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  commentModalStatus: false,
};

export const CommentModalSlice = createSlice({
  name: "commentModal",
  initialState,
  reducers: {
    handleOpeningCommentModal: (state) => {
      state.commentModalStatus = true;
    },
    handleClosingCommentModal: (state) => {
       state.commentModalStatus = false;
    },
  },
});

export const { handleOpeningCommentModal, handleClosingCommentModal } = CommentModalSlice.actions;

export default CommentModalSlice.reducer;
