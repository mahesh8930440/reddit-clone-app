import {createApi,fakeBaseQuery} from "@reduxjs/toolkit/query/react"
import { addDoc, collection ,getDocs,getDoc} from 'firebase/firestore';
import {db} from "../firebaseAuthentication"
export const communityApi=createApi({
    reducerPath:"communityApi",
    baseQuery:fakeBaseQuery(),
    tagTypes:["community"],
    endpoints:(builder) =>({
        getCommunity:builder.query({
            async queryFn(){
                try{
                    const subRedditsSnapshot = await getDocs(collection(db, "subReddit"));
                    const data = subRedditsSnapshot.docs.map((doc) => doc.data());
                    console.log(data)
                    return {data};
                }
                catch(err){
                    console.log("An error occurred:",err)
                   return {error:err}
                    
                }
            },
            providesTags:["community"]
        }),
        addCommunity: builder.mutation({
            async queryFn(data) {
              try {
                const result = await new Promise((resolve, reject) => {
                  const timeoutId = setTimeout(async () => {
                    if (!navigator.onLine) {
                      console.log("You are currently offline. Please check your network connection.");
                      reject(new Error("You are currently offline. Please check your network connection."));
                      return;
                    }
          
                    try {
                      await addDoc(collection(db, "subReddit"), {
                        name: data,
                      });
                      resolve({ data: "ok" });
                    } catch (error) {
                      reject(error);
                    }
                  }, 5000);
                });
          
                return result;
              } catch (err) {
                console.log( err);
                throw err; 
              }
            },
            invalidatesTags: ["community"],
          }),
          
       
    })
    
})


export const {useGetCommunityQuery,useAddCommunityMutation}=communityApi;