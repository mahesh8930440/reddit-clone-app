import {  createApi, fakeBaseQuery} from "@reduxjs/toolkit/query/react";
import { addDoc, collection ,getDocs,getDoc,doc, serverTimestamp,setDoc} from 'firebase/firestore';

import {db} from "../firebaseAuthentication"
export const createPostApi=createApi({
    reducerPath:"createPostApi",
    baseQuery:fakeBaseQuery(),
    tagTypes:["createPost"],
    endpoints:(builder)=> ({
       fetchPost:builder.query({
          async queryFn(){
            try{
               const postSnapShot=await getDocs(collection(db, "posts"));
               const postData=postSnapShot.docs.map((doc)=>doc.data())
               console.log(postData)
               return {data: postData}
            }
            catch(err){
               console.log("An error occurred:",err)
               return {error:err}
               
            }
            
          },
          providesTags:["createPost"]
       }),
       addPost:builder.mutation({
          async queryFn(data){
            try{
                console.log(data.postId)
                const {postId} =data ;
                const postRef=doc(db,"posts",postId)
                await setDoc(postRef,data)
                console.log("ok")
                return {data:"ok"}
            }
            catch(err){
               console.log("An error occurred:",err)
               return {error:err}
            }
           
          },
          invalidatesTags:["createPost"]
       })
    })
})
export const {useFetchPostQuery,useAddPostMutation}=createPostApi;