import * as React from 'react'
import { ChakraProvider ,Box} from '@chakra-ui/react'
import Header from "./Componets/Header"
import {createBrowserRouter,Outlet} from "react-router-dom"
import SubReddit from './pages/SubReddit'
import Comments from './pages/Comments'
import Posts from './pages/Posts'
import CreatePost from './pages/CreatePost'
function App() {
  
    return (
      <ChakraProvider>
        <Header />
        <Box minH={"90vh"}  bgColor={"#F1F5F9"}>
           <Outlet />
        </Box>
     
      </ChakraProvider>
      
    )
  
}

const appRouter=createBrowserRouter([
  {
    path:"/",
    element:<App/>,
    children:[
      {
        path:"/",
        element:<Posts/>      
      },
      {
        path:"/submit",
        element:<CreatePost />
      },
      {
        path:"/r/:id",
        element:<SubReddit/>
      },
      {
        path:"/r/:subredditName/comment/:postId",
        element:<Comments/>
      }
      
    ]
  }
])

export default appRouter;