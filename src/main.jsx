import React from 'react'
import ReactDOM from 'react-dom/client'
import appRouter from './App.jsx'
import { FirebaseProvider } from './firebaseAuthentication.jsx'
import { store } from './Redux/store.jsx'
import { Provider } from 'react-redux'
import {RouterProvider} from "react-router-dom"
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <FirebaseProvider>
         <RouterProvider router={appRouter}/>
      </FirebaseProvider> 
    </Provider>
  </React.StrictMode>,
)
