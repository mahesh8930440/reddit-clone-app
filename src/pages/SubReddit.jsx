
import { useLocation } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import {Flex,Heading,Box,Text, Input,Avatar,AvatarBadge,Card,CardBody,CardHeader,Stack,StackDivider,Icon,Image,Button} from "@chakra-ui/react"
import {Link} from "react-router-dom"
import { LinkIcon } from "@chakra-ui/icons";
import { BiDownvote } from "react-icons/bi";
import { BiUpvote } from "react-icons/bi";
import { IoChatboxOutline } from "react-icons/io5";
import { useState,useEffect } from 'react';
import { useFetchPostQuery } from "../Redux/CreatePostApi";
import { useUpdateVoteCountMutation } from '../Redux/CreateVoteApi';
import { formatDistanceToNow } from "date-fns";
const subReddit=()=>{
    const {id}=useParams();
    const [currentPage, setCurrentPage] = useState(1);
    const [sortedPosts, setSortedPosts] = useState([]);
    const [sortBy, setSortBy] = useState('');
    const itemsPerPage = 5;
    let name;
    
    const {  data, isLoading, isError, error,refetch: refetchPostData  } = useFetchPostQuery();
    const [updatedVoteCount]=useUpdateVoteCountMutation();
    const subRedditPostData=data ?.filter((post) => post.subredditName===id) 
    console.log(subRedditPostData)
    if(id!=="undefined"){
      name=id
    }
    else{
        const location = useLocation();
        console.log(location.name)
        name=(location.name)
    }
    const handleVote= async (newVoteType, postId, voteCount)=>{
        try{
            await updatedVoteCount({newVoteType, postId, voteCount})
            await refetchPostData();
        }
        catch(err){
            console.log(err)
        }
    }
    useEffect(()=>{
        if(subRedditPostData){
           setSortedPosts(subRedditPostData)
           setSortBy("")
        }
        
    },[name,data])
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    const currentSubRedditPostItems = sortedPosts?.slice(startIndex, endIndex);
    const handleLatestPost = () => {
      const sortedPostData = subRedditPostData.slice().sort((newPost, oldPost) => oldPost.createdAt - newPost.createdAt);
      setSortedPosts(sortedPostData);
      setSortBy('latest');
      console.log("latest");
    };
  
    const handlePopularPost = () => {
      const sortedPostData = subRedditPostData.slice().sort((post, old) => old.voteCount - post.voteCount);
      setSortedPosts(sortedPostData);
      setSortBy('popular');
      console.log("votes");
    };
  
    const totalPages = Math.ceil(subRedditPostData?.length / itemsPerPage);
    
    const handlePageChange = (newPage) => {
      if (newPage >= 1 && newPage <= totalPages) {
        setCurrentPage(newPage);
      }
    };
    
    return (
    <Box py="2%"  maxW="50%" mx="auto">
        <Flex textAlign={"center"} justifyContent={"center"} bg="honeydew" py="20px" my="10px">
        
                <Avatar src={"https://images.pexels.com/photos/1704488/pexels-photo-1704488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"} height="40px" width="40px" mr="10px">
                </Avatar>
                <Box>
                    <Heading>{name}</Heading>
                    <Text fontSize={"1rem"}>{`/r/${name}`}</Text>
                </Box>
        </Flex>
        
        <Flex justifyContent="space-between"  mb="40px" bgColor={"white"} p={"20px"} borderRadius="md">
            <Avatar mr="4%" src="https://wallpaperaccess.com/full/2213441.jpg">
            <AvatarBadge boxSize="1.25em" bg="green.500" />
            </Avatar>
            <Link to="/submit">
            <Input
                placeholder="Create Post"
                size="lg"
                mr="4%"
                p="10px"
                w="35vw"
            />
            </Link>
            <LinkIcon alignSelf={"center"} />
        </Flex>
        {currentSubRedditPostItems.length>0 ? 
            (<Flex  justifyContent={"center"}>
                <Button mx="20px" colorScheme={sortBy === 'latest' ? 'teal' : 'gray'} onClick={handleLatestPost}>Latest</Button>
                <Button mx="20px" colorScheme={sortBy === 'popular' ? 'teal' : 'gray'} onClick={handlePopularPost}>Popular</Button>
            </Flex>):(<Text  textAlign={"center"}>No post</Text>)}
        <Flex direction={"column"} spacing="4" mt="20px" size="lg" w="100%">
                {currentSubRedditPostItems?.map((data) => {
                const dateObject = data.createdAt.toDate();
                const formattedDate = formatDistanceToNow(dateObject, {
                    addSuffix: true,
                });

                return (
                    <Card size="md" mb="20px" width="100%">
                    <Flex>
                        <Box id="data.postId" my="10px" bg="whitesmoke" p="10px">
                        <BiUpvote
                            size="30px"
                            color={data.voteType === "upVote" ? "red" : "gray"}
                            onClick={() =>
                                handleVote("upVote", data.postId, data.voteCount)
                            }
                        />
                        <Text id="data.postId" textAlign={"center"}>
                            {data.voteCount}
                        </Text>
                        <BiDownvote
                            size="30px"
                            color={data.voteType === "downVote" ? "red" : "gray"}
                            onClick={() =>
                                handleVote("downVote", data.postId, data.voteCount)
                            }
                        />
                        </Box>
                        <Box display={"column"}>
                        <CardHeader>
                            <Flex justifyContent={"space-between"}>
                            <Flex mr="20px">
                                <Avatar bg="teal.500" h="25px" w="25px" mr="10px" src="https://enviragallery.com/wp-content/uploads/2015/12/background-lighting.jpg"/>
                                <Heading size="sm" fontSize="md">{data.subredditName}</Heading>
                            </Flex>

                            <Text mr="20px" fontSize="md" textColor={"gray"}>Post by {data.userName}</Text>
                            <Text textColor={"gray"}>{formattedDate}</Text>
                            </Flex>
                        </CardHeader>

                        <CardBody>
                            <Stack divider={<StackDivider />} spacing="2">
                            <Box>
                                <Heading size="md" textTransform="uppercase">
                                {data.title}
                                </Heading>
                            
                                <Text pt="2" fontSize="md">
                                {data.description && data.description.startsWith("http") ? (
                                    <a href={data.description} target="_blank" rel="noopener noreferrer" style={{ color: 'rgb(96 165 250)', textDecoration: 'underline' }}>
                                    {data.description}
                                    </a>
                                ) : (
                                    <>
                                    <Text pt="2" fontSize="md">
                                        {data.description}
                                    </Text>
                                    {data.imageUrl && <Image src={data.imageUrl} alt="Image" />}
                                    </>
                                )}
                                </Text>
                                                        

                            </Box>
                            </Stack>
                        </CardBody>
                        <CardBody>
                            <Link to={`/r/${data.subredditName}/comment/${data.postId}`}>
                                <Flex >
                                    <Icon as={IoChatboxOutline} mr="10px" size={"md"}/>
                                    <Text textAlign={"center"}>{data.commentCount} Comment</Text>
                                </Flex>
                            </Link>
                            
                        </CardBody>
                        </Box>
                    </Flex>
                    </Card>
                );
                })}
            </Flex>
            {currentSubRedditPostItems.length>0 && 
            (<Box color={"gray"} mx="24px" my="24px">
                <Button disabled={currentPage === 1} mx="24px" onClick={() => handlePageChange(currentPage - 1)}>
                Previous
                </Button>
                <span mx="24px" >{`Page ${currentPage} of ${totalPages}`}</span>
                <Button disabled={currentPage === totalPages} mx="24px" onClick={() => handlePageChange(currentPage + 1)}>
                Next
                </Button>
            </Box>)}
    </Box>    
   )
}
export default subReddit