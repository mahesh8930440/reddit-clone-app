import {Container,Box,Heading, Tabs,TabList,HStack,Icon,TabPanel,TabPanels,Input,Textarea,Text,Button,ButtonGroup,Tab,Select} from "@chakra-ui/react"
import { useGetCommunityQuery } from '../Redux/subRedditApi';
import {Link} from "react-router-dom"
import { v4 as uuidv4 } from 'uuid';
import {useContext} from "react";
import { FirebaseContext } from "../firebaseAuthentication";
import { LinkIcon } from "@chakra-ui/icons";
import { useNavigate } from "react-router-dom";
import { FaExternalLinkAlt } from "react-icons/fa";
import { storage } from "../firebaseAuthentication";
import { ref , uploadBytes, getDownloadURL } from "firebase/storage";
import { serverTimestamp } from "firebase/firestore";
import { useState } from "react";
import { useAddPostMutation} from "../Redux/CreatePostApi";

const CreatePost=()=>{
    const navigate = useNavigate();
    const [addPost]=useAddPostMutation();
    const {user}=useContext(FirebaseContext) ;
    const [title,setTitle]=useState("");
    const [text,setText] =useState("");
    const [file, setFile] = useState(null);
    const [isLoading,setIsLoading]=useState(false);
    const [selectedPostType, setSelectedPostType] = useState("Post");
    const [selectedCommunity,setSelectedCommunity] =useState("");
    const {data,error} =useGetCommunityQuery();
    const [errorMessage,setErrorMessage]=useState("");
    const handlePost = async () => {
        try {
          
          const postId = new Date().getTime().toString();
          let postData = {
                postId,
                title,
                userId: user.uid,
                userName: user.displayName,
                subredditName: selectedCommunity,
                createdAt: serverTimestamp(),
                editedAt: serverTimestamp(),
                voteCount: 0,
                commentCount: 0,
                voteType: null,
            };
          
          const uploadFileToStorage = async (file) => {
            try {
              
              const storageRef = ref(storage, `uploads/${uuidv4()}`);
               
              const uploadTask = await uploadBytes(storageRef, file);
            
              const snapshot = await getDownloadURL(uploadTask.ref);
              
              return snapshot;
            } catch (error) {
              console.error("Error uploading file:", error.message);
              throw error;
            }
          };
          if (selectedPostType === "Post") {
            postData = { ...postData, description: text };
          } else if (selectedPostType === "Upload") {
             if (file){
              
              const fileUrl = await uploadFileToStorage(file);
              postData = { ...postData, imageUrl: fileUrl };
             }
                
          } else if (selectedPostType === "Link") {
            postData = { ...postData, description: text };
          }
          if (!navigator.onLine) {
            setErrorMessage(
              "You are currently offline. Please check your network connection."
            );
            return;
          }
          else if (!selectedCommunity) {
            setErrorMessage("Community name is required.");
            return;
          }
          else if(!title){
            setErrorMessage("Title is required.");
            return;
          }
          setIsLoading(true);
          await addPost(postData);

          setIsLoading(false);
         
          navigate(`/r/${selectedCommunity}/comment/${postId}`);
          setSelectedCommunity("");
          setText("");
          setTitle("");
          
          setErrorMessage("");
          
        } catch (err) {
          console.log(err.message);
          setIsLoading(false);
        }
      };
     
      
      const handleFileUpload = (e) => {
        const uploadedFile = e.target.files[0];
        setFile(uploadedFile);
      };
    return (
        <Container  pt="30px"  maxW='2xl' >
            <Box bg="white" p="30px" borderRadius={"md"}>
            <Heading mb="20px" fontSize={"1.5rem"}>Create a Post</Heading>
            <Select placeholder='Choose Your Community' value={selectedCommunity} mb="30px" onChange={(e)=> {setSelectedCommunity(e.target.value),setErrorMessage("")}}>
                {data && (
                    data?.map((community) => (
                        <option value={community.name}>
                            {community.name}
                        </option>
                    ))
                )}
            </Select>
            
            <Tabs isFitted variant='enclosed'>
                <TabList mb='1em'>
                <Tab onClick={() => setSelectedPostType("Post")} isSelected={selectedPostType === "Post"}>
                    <HStack spacing={2}>
                    <Icon as={FaExternalLinkAlt} boxSize={4} />
                    <Text>Post</Text>
                    </HStack>
                </Tab>
                <Tab onClick={() => setSelectedPostType("Upload")} isSelected={selectedPostType === "Upload"}>
                    <HStack spacing={2}>
                    <LinkIcon alignSelf={"center"} />
                    <Text>Upload</Text>
                    </HStack>
                </Tab>
                <Tab onClick={() => setSelectedPostType("Link")} isSelected={selectedPostType === "Link"}>
                    <HStack spacing={2}>
                    <LinkIcon alignSelf={"center"} />
                    <Text>Link</Text>
                    </HStack>
                </Tab>
                </TabList>
                <TabPanels>
                <TabPanel>
                    <Input placeholder='Title' mb="20px" value={title} onChange={(e) => {setTitle(e.target.value),setErrorMessage("")}} />
                    <Textarea placeholder='Text' mb="20px" value={text} onChange={(e) => {setText(e.target.value),setErrorMessage("")}} />
                </TabPanel>
                <TabPanel>
                    <Input placeholder='Title' mb="20px" value={title} onChange={(e) => {setTitle(e.target.value),setErrorMessage("")}} />
                    <Input type="file" mb="20px" onChange={(e) => handleFileUpload(e)} />
                </TabPanel>
                <TabPanel>
                    <Input placeholder='Title' mb="20px" value={title} onChange={(e) => {setTitle(e.target.value),setErrorMessage("")}} />
                    <Input placeholder='URL' mb="20px" value={text}  onChange={(e) => {setText(e.target.value),setErrorMessage("")}}/>
                </TabPanel>
                </TabPanels>
            </Tabs>
            {errorMessage && <Text color={"red"} ml="1%" my="2%">{errorMessage}</Text>}
            <ButtonGroup variant='outline' spacing='6'  >
                <Button colorScheme='blue'  onClick={() => handlePost()} loadingText="loading" isLoading={isLoading} >Save</Button>
                <Button onClick={()=> navigate("/")}>Cancel</Button>
            </ButtonGroup>
            </Box>  
        </Container>
    )

}
export default CreatePost