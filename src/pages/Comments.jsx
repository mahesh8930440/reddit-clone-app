import { useParams } from "react-router-dom";
import {Box,Flex,Card,Text,CardHeader,Heading,Avatar,Stack,CardBody,StackDivider,Image,Icon,Textarea,Button, Spacer} from "@chakra-ui/react"
import { LinkIcon } from "@chakra-ui/icons";
import { IoChatboxOutline } from "react-icons/io5";
import { FaRegEdit } from "react-icons/fa";
import { BiDownvote } from "react-icons/bi";
import { BiUpvote } from "react-icons/bi";
import { FirebaseContext } from '../firebaseAuthentication';
import {useContext} from "react";
import { formatDistanceToNow } from "date-fns";
import { useFetchCommentQuery,useFetchPostQuery,useAddCommentsMutation } from "../Redux/CreateCommentApi";
import { useUpdateVoteCountMutation } from "../Redux/CreateVoteApi";
import { useState,useEffect } from "react";
import CommentModal from "../Componets/CommentModal";
import { useSelector, useDispatch } from 'react-redux'
import { handleOpeningCommentModal } from "../Redux/CommentModalSlice";
const Comments=()=>{
    const modalStatus = useSelector((state) => state.commentModal.commentModalStatus)
    const dispatch = useDispatch()
    const { postId, subredditName } = useParams();
    const authData =useContext(FirebaseContext);
    const [isLoading,setIsLoading]=useState(false);
    const user=authData.user;
    const  {data:post,refetch: refetchPostData}=useFetchPostQuery(postId);
    const [days, setDays] = useState(null);
    const [addComments]= useAddCommentsMutation();
    const [updatedVoteCount]=useUpdateVoteCountMutation();
    const [inputValue,setInputValue]=useState("");
    const {data:commentsData}=useFetchCommentQuery(postId);
    const [errorMessage,setErrorMessage]=useState("");
    useEffect(() => {
        if (post && post.createdAt) {
          const dateObject = post.createdAt.toDate();
          const formattedDate = formatDistanceToNow(dateObject, {
            addSuffix: true,
          });
          setDays(formattedDate);
        }
      }, [post]);
    const handleComment=async(post)=>{
        try{
          if (!navigator.onLine) {
            setErrorMessage(
              "You are currently offline. Please check your network connection."
            );
            return;
          }
          if (!inputValue) {
            setErrorMessage("Community name is required");
            return;
          } 
          console.log(inputValue)
          setErrorMessage("")
          setIsLoading(true);
          await addComments({post,inputValue})  
          setIsLoading(false);
          setInputValue("")
          
        }
        catch(err){
          console.log("An error occurred")
          setErrorMessage("An error occurred")
        }  
      }
    const handleVote= async (newVoteType, postId, voteCount)=>{
        try{
            await updatedVoteCount({newVoteType, postId, voteCount})
            await refetchPostData()
        }
        catch(err){
            console.log(err)
        }
    }
    return ( 
        <Box   maxW="40%" mx="auto">
            {post && (
            
            <Flex my="30px" pt="40px"  mx="auto">
                <Card size="md" mb="20px" width="100%">
                <Flex>
                    <Box id="post.postId" my="10px" bg="whitesmoke" p="10px">
                    <BiUpvote
                        size="30px"
                        color={post.voteType === "upVote" ? "red" : "gray"}
                        onClick={() =>
                        handleVote("upVote", post.postId, post.voteCount)
                        }
                    />
                    <Text id="post.postId" textAlign={"center"}>
                        {post.voteCount}
                    </Text>
                    <BiDownvote
                        size="30px"
                        color={post.voteType === "downVote" ? "red" : "gray"}
                        onClick={() =>
                        handleVote("downVote", post.postId, post.voteCount)
                        }
                    />
                    </Box>
                    <Box display={"column"}>
                    <CardHeader>
                        <Flex justifyContent={"space-between"}>
                        <Flex mr="20px">
                            <Avatar bg="teal.500" h="25px" w="25px" mr="10px" src="https://wallpaperaccess.com/full/2213441.jpg"/>
                            <Heading size="sm">{post.subredditName}</Heading>
                        </Flex>

                        <Text mr="20px" color="gray" size="sm">Post by {post.userName}</Text>
                        <Text color="gray" size="sm">{days}</Text>
                        </Flex>
                    </CardHeader>

                    <CardBody>
                        <Stack divider={<StackDivider />} spacing="4" mb="20px">
                        <Box>
                            <Heading size="md" textTransform="uppercase">
                            {post.title}
                            </Heading>
                            <Text pt="2" fontSize="md">
                                {post.description && post.description.startsWith("http") ? (
                                    <a href={post.description} target="_blank" rel="noopener noreferrer" style={{ color: 'rgb(96 165 250)', textDecoration: 'underline' }}>
                                    {post.description}
                                    </a>
                                ) : (
                                    <>
                                    <Text pt="2" fontSize="md">
                                        {post.description}
                                    </Text>
                                    {post.imageUrl && <Image src={post.imageUrl} alt="Image" />}
                                    </>
                                )}
                                </Text>
                        </Box>
                        </Stack>
                        <Flex>
                        <Icon as={IoChatboxOutline} mr="10px" />
                        <Text color={"gray"}>{post.commentCount} Comment</Text>
                        </Flex>
                    </CardBody>
                    
                    <CardBody >
                    {user && <Text mb='8px'>comment as    <Box as="span" color="pink">{user.displayName}</Box></Text>}
                    <Textarea
                        value={inputValue}
                        onChange={(e)=>  {setInputValue(e.target.value),setErrorMessage("")}}
                        placeholder='what are you thoughts ?'
                        size="md"
                        mb="40px"
                        variant='filled'
                    />
                    {errorMessage && <Text color={"red"}>{errorMessage}</Text>}
                    <Button colorScheme='blue' onClick={()=>handleComment(post,inputValue)} loadingText="loading" isLoading={isLoading}>comment</Button>
                    </CardBody>
                    </Box>
                </Flex>
                </Card>
            </Flex>
            )}
            
            
            { commentsData?.map((comment) =>{
               
            return(<Box p='30px' bg="white" borderRadius={"md"} my="20px">
                <Flex mr="20px" textAlign={"center"}>
                    <Avatar bg="teal.500" h="25px" w="25px" mr="10px" src="https://enviragallery.com/wp-content/uploads/2015/12/background-lighting.jpg"/>
                    <Heading size="md" mr="10px">{comment.user}</Heading>
                    <Spacer/>
                    {(user.displayName===comment.user) && (<FaRegEdit onClick={()=>dispatch(handleOpeningCommentModal())}/>)}
                    {modalStatus && (<CommentModal comment={comment}/>)}
                </Flex>
                
                <Text mb="20px" ml="40px">{comment.description}</Text>
            </Box>)})
            
            }
           
       </Box>
    )
}
export default Comments