import {createContext} from "react";
import {initializeApp} from "firebase/app";
import {getAuth,GoogleAuthProvider,signInWithPopup,onAuthStateChanged} from  'firebase/auth'
import {useState,useEffect} from "react";
import { getFirestore } from 'firebase/firestore';
import { getStorage } from "firebase/storage";
import { signOut } from 'firebase/auth';

import { useToast } from "@chakra-ui/react";
const firebaseConfig = {
    apiKey: "AIzaSyBevS1AeaCt-3RcKik96FBkUSlRO0CZCTU",
    authDomain: "reddit-react-redux-clone.firebaseapp.com",
    projectId: "reddit-react-redux-clone",
    storageBucket: "reddit-react-redux-clone.appspot.com",
    messagingSenderId: "829420246747",
    appId: "1:829420246747:web:dbe954f91c010808d41efe",
    measurementId: "G-XEBTQLDKTZ"
  };
  
 

const app= initializeApp(firebaseConfig);
export const db=getFirestore(app)
export const storage=getStorage(app)
export const auth=getAuth(app);
const googleProvider =new GoogleAuthProvider();
export const FirebaseContext= createContext(null);

export const FirebaseProvider = (props)=>{
    const [user,setUser] =useState(null);
    const toast =useToast();
    const signInWithGoogle=async ()=>{
        try{
            await signInWithPopup(auth,googleProvider)
            toast({
                title: 'LogIn',
                description: 'You have been successfully logged .',
                status: 'success',
                duration: 2000,
                isClosable: true,
        })}
        catch(err){
            toast({
                title: 'Error',
                description: err.message,
                status: 'failed',
                duration: 2000,
                isClosable: true,
        }) 
        }
    
       
    }
    const handleSignOut = async () => {
        
        try {
            await signOut(auth);
            toast({
                title: 'Logout',
                description: 'You have been successfully logged out.',
                status: 'success',
                duration: 2000,
                isClosable: true,
            })
            console.log("sign out success")
        } catch (error) {
            toast({
                title: 'Error',
                description: error.message,
                status: 'failed',
                duration: 2000,
                isClosable: true,
            })
            console.error('Error signing out:', error);
        }
    };
    useEffect(()=>{
        const unsubscribe = onAuthStateChanged(auth,(user)=>{
            if(user){
                setUser(user);
            }
            else{
                setUser(null);
            }

        });
        return () => {
            unsubscribe();
        };
    },[])
    return (
         <FirebaseContext.Provider value={{signInWithGoogle,user,handleSignOut}}>
            {props.children}
         </FirebaseContext.Provider>
    )
}